from keras.utils import plot_model
from keras.models import Model
from keras.layers import Input
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D
from keras.layers import BatchNormalization
import scipy.io as sio


visible = Input(shape=(3,25,1))

conv1 = Conv2D(16, kernel_size=(1,2), activation='relu')(visible)
bn1 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(conv1)
hidden1 = Dense(1600, activation='relu')(bn1)
pool1 = MaxPooling2D(pool_size=(1, 3),strides = (1,2))(hidden1)

conv2 = Conv2D(32, kernel_size=(1,3), activation='relu')(pool1)
bn2 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(conv2)
hidden2 = Dense(1280, activation='relu')(bn2)

conv3 = Conv2D(40, kernel_size=(1,3), activation='relu')(hidden2)
bn3 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(conv3)
hidden3 = Dense(1280, activation='relu')(bn3)

conv4 = Conv2D(40, kernel_size=(1,3), activation='relu')(hidden3)
bn4 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(conv4)
hidden4 = Dense(960, activation='relu')(bn4)

conv5 = Conv2D(40, kernel_size=(1,3), activation='relu')(hidden4)
bn5 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(conv5)
hidden5 = Dense(640, activation='relu')(bn5)

hidden6 = Dense(640, activation=None)(hidden5)
bn6 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(hidden6)
hidden7 = Dense(40, activation='relu')(bn6)

hidden8 = Dense(40, activation=None)(hidden7)
bn7 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(hidden8)
hidden9 = Dense(40, activation='relu')(bn7)

hidden8 = Dense(40, activation=None)(hidden9)
bn8 = BatchNormalization(axis=1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(hidden9)
hidden9 = Dense(1, activation='relu')(bn8)


output = Dense(1, activation='relu')(hidden9)
model = Model(inputs=visible, outputs=output)




# summarize layers
print(model.summary())
# plot graph
plot_model(model, to_file='convolutional_neural_network.png')
